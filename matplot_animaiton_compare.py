"""
A simple example of an animated plot
"""
import numpy as np
import csv
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib.pyplot import gca
import time


# take a part of the total data if needed
time_length = 10000 #[seconds]
# font size for the labels and legend
font_size = 15 

# Define a time variable to measure the time necessary to realize the video
t = time.time()

# Initialise arrays
time_csv = []
data_csv = []

# Open a csv file containing the timestamps and the data
with open('data.csv') as csvfile:
    spamreader = csv.reader(csvfile, delimiter=',')
    for row in spamreader:
        time_csv.append((float(row[0])))
        data_csv.append(float(row[1]))


#Offset timestamps at 0 (if necessary)
time_csv = np.array([x-time_csv[0] for x in time_csv])
data_csv = np.array(data_csv)

# downsample if needed
# time_csv = time_csv[::10]
# data_csv = data_csv[::10]

# Define the data and scale it
data_csv = data_csv[time_csv < time_length]
#To stop the plot at a certain time
time_csv = time_csv[time_csv < time_length]

#Create figure as you like
fig = plt.figure(num=1, figsize=(18,8), dpi=80, facecolor='w', edgecolor='k')

# Define plot and its limits
ax1 = plt.subplot(1,1,1)
ax1.set_xlim([0.0,time_csv[-1]])
ax1.set_ylim([0.0,max(data_csv)*1.1])
ax1.set_ylabel('Y label (unit)',fontsize=font_size)
ax1.set_xlabel('X label (unit)',fontsize=font_size)
ax1.tick_params(axis='both',labelsize=font_size, bottom=False)
ax1.grid(True);
ax1.spines['bottom'].set_color('0.3')
ax1.spines['top'].set_color('0.3')
ax1.spines['right'].set_color('0.3')
ax1.spines['left'].set_color('0.3')

# add an additional static line (e.g. a dome threshold)
half_line = np.ones((len(data_csv)))*0.5*12
line1, = ax1.plot(time_csv,data_csv,linewidth=2.0,color='blue', label="$y$ some label")
line2, = ax1.plot(time_csv,half_line,linewidth=2.0, color='black', linestyle='dashed',label="$T$ some treshold")
ax1.legend(fontsize=font_size)

# filling between the lines
compare = ax1.fill_between(time_csv, half_line,half_line, alpha=0.6, facecolor='tab:red', interpolate=True, label="above treshold")

#define markers for the animation
marker1, = ax1.plot(time_csv,data_csv,marker='.',markersize=14,color='blue');
marker2, = ax1.plot(time_csv,half_line,marker='.',markersize=14,color='black');

# Init required to give a clean slate.
def init():
    line1.set_data([],[])
    marker1.set_data([],[])
    line2.set_data([],[])
    marker2.set_data([],[])
    return line1, marker1,line2, marker2


def animate(frame):
    line1.set_data(time_csv[0:frame],data_csv[0:frame])  # update the data
    marker1.set_data(time_csv[frame-1:frame],data_csv[frame-1:frame])
    line2.set_data(time_csv[0:frame],half_line[0:frame])  # update the data
    marker2.set_data(time_csv[frame-1:frame],half_line[frame-1:frame])

    # update the fill_between polygon
    v = compare.get_paths()[0].vertices
    if data_csv[frame] > half_line[frame]:
        v[frame, 1] = data_csv[frame]
    else:
        v[frame, 1] = half_line[frame]

    return line1, marker1, line2, marker2, compare

# Animate and show the plot
anim = animation.FuncAnimation(fig, animate, init_func=init, frames=np.size(time_csv), interval=1, blit=True, repeat=False)
# plt.show()

# Define framerate according to timestamps
frameRate1 = 1.0/(time_csv[-1]/np.size(time_csv))
# Define filename and save it as a mp4 file
file_name = './video_animation_compare.mp4'
anim.save(file_name, fps=frameRate1, extra_args=['-vcodec', 'libx264'])

# Print time necessary to realize the video
print(time.time() - t)

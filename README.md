# Example of creeating mp4 videos using matplotlib from csv data




Two example scripts provided 

- `matplotlib_animation.py` - creating the animation form the `data.csv` file 
- `matplotlib_animation_compare.py` - creating the animation form the `data.csv` by adding a treshold and filling between the data and the treshold

To run the scripts call

```
python matplotlib_animation.py
```

or 

```
python matplotlib_animation_compare.py
```

The result of the program will be visualised in a form of the figure and once you close it the program will save it to the `mp4` file.

![](example.gif)


To turn the `mp4` file into `gif` file you can use `ffmpeg`:

```
ffmpeg -i video_animation_compare.mp4 -filter_complex 'fps=10,scale=-1:-1:flags=lanczos,split [o1] [o2];[o1] palettegen [p]; [o2] fifo [o3];[o3] [p] paletteuse' out.gif
``````